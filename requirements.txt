mkdocs~=1.0
pygments~=2.0,>=2.4
mkdocs-material~=4.0
mkdocs-minify-plugin
mkdocs-git-revision-date-localized-plugin
