title: Introduction

# The Raven Authentication Service

The Raven Authentication Service provides a secure means to allow sites within
the University to authenticate the majority of University members without
directly handling user passwords.

## Starting points

<div class="card-container">
  <a href="first-steps" class="card">
    <div class="card-icon">
      <i class="material-icons">
        school
      </i>
    </div>
    <div class="card-content">
      <h3>Quickstart</h3>
      Protect your first site with Raven
    </div>
  </a>
  <a href="golden-rules" class="card">
    <div class="card-icon">
      <i class="material-icons">
        check_circle
      </i>
    </div>
    <div class="card-content">
      <h3>Verify</h3>
      "Golden Rules" to secure your site
    </div>
  </a>
  <a href="overview" class="card">
    <div class="card-icon">
      <i class="material-icons">
        compare
      </i>
    </div>
    <div class="card-content">
      <h3>Compare</h3>
      Raven authentication protocols
    </div>
  </a>
  <a href="develop" class="card">
    <div class="card-icon">
      <i class="material-icons">
        code
      </i>
    </div>
    <div class="card-content">
      <h3>Develop</h3>
      Adding Raven to your applications
    </div>
  </a>
  <a href="reference-apache-oauth2" class="card">
    <div class="card-icon">
      <i class="material-icons">
        settings
      </i>
    </div>
    <div class="card-content">
      <h3>Configure</h3>
      Apache with Raven OAuth2
    </div>
  </a>
  <a href="oauth2-claims" class="card">
    <div class="card-icon">
      <i class="material-icons">
        book
      </i>
    </div>
    <div class="card-content">
      <h3>Reference</h3>
      OAuth2 Claims
    </div>
  </a>
  <a href="reference-apache-saml2" class="card">
    <div class="card-icon">
      <i class="material-icons">
        settings
      </i>
    </div>
    <div class="card-content">
      <h3>Configure</h3>
      Apache with Raven SAML 2.0
    </div>
  </a>
  <a href="saml2-attributes" class="card">
    <div class="card-icon">
      <i class="material-icons">
        book
      </i>
    </div>
    <div class="card-content">
      <h3>Reference</h3>
      SAML 2.0 Attributes
    </div>
  </a>
</div>

## About Raven

Raven is a service provided by [University of Cambridge Information
Services](https://www.uis.cam.ac.uk/). It is managed by [UIS
DevOps](http://guidebook.devops.uis.cam.ac.uk/).
