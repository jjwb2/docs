title: OAuth2 Client Credentials

# Registering OAuth2 Client Credentials

The Raven OAuth2 service allows websites to authenticate using the
[OAuth2](https://oauth.net/2/) protocol. In order to make use of the OAuth2
protocol you will need to register some *client credentials*.

!!! important

    The user experience for Raven OAuth2 may differ from the user experience
    shown by legacy Raven services. In particular users will be asked for
    consent on the first sign in. Please see the [notice to IT
    professionals](https://help.uis.cam.ac.uk/service/support/help-for-institutions/google-cloud-identity)
    on the UIS website for more information.

OAuth2 requires that an application authenticating a user identify itself to the
authentication service. Client credentials are the way in which your website
identifies itself to Raven when requesting user sign in.

Client credentials consist of two parts. The **client id**, sometimes called the
**client key**, is the equivalent of a username identifying your application.
The **client secret** is the equivalent of a password. As the name suggests, it
should be kept secret.

## Google account types

Raven makes use of OAuth2 client credentials managed by Google. There are,
somewhat confusingly, several forms of identity in the Google ecosystem:

An **ordinary Google account** is the sort of Google account you can register
for free at https://google.com/. All that you need to register an ordinary
Google account is the ability to receive email. An ordinary Google account is
associated with an email address.

A **Google Workspace account** is like an ordinary Google account but (for the
University of Cambridge tenacy) the associated email address ends in
`@cam.ac.uk`. Google Workspace accounts use Raven to authenticate rather than
giving a password to Google. A Google Workspace account is associated with a
single individual.

A **service account** is an account associated with a particular “project” in
Google’s [Cloud console](https://console.cloud.google.com/). They are
_not_ associated with individuals. They do have email addresses associated with
them which end with `.gserviceaccount.com`. There is no mailbox behind the
email address associated with service accounts.

## Which account to use

In order to register new OAuth2 client credentials you will need to create a
project in Google Cloud. Which account you use to create this account depends
on the purpose of the site.

If the website you are configuring is an official University (not College) site
and you are a member of staff (marked as 'staff' in Lookup) then you will be
able to create projects using your CRSid@cam.ac.uk *Google Workspace account*.

If it does not already exist, a folder for your institution in the `cam.ac.uk`
Google Cloud organisation will need creating (by request to
[servicedesk@uis.cam.ac.uk](mailto:servicedesk@uis.cam.ac.uk)). You can tell if
this is the case by signing in to the
[Cloud Resource Manager](https://console.cloud.google.com/cloud-resource-manager)
(with your CRSid@cam.ac.uk account) and looking for an appropriately named
institution folder under the `cam.ac.uk > Institutions` folder.

!!! important "Enable Google Cloud Platform"
    Depending on what they're doing, Google Workspace users may have to [opt in
    to Google Cloud Platform](https://preferences.g.apps.cam.ac.uk/) for their
    account.

If the website you are configuring is a College site, for experimenting or for
any personal reason, then you will need to create an *ordinary Google account*
(or use an existing one). We **strongly** recommend that you create a shared
ordinary Google account for a role address. For example, the COs of St Botolph's
college would create a Google account for `computer-officers@botolphs.cam.ac.uk`
and store the credentials like they do with any other shared secrets.
By using a role address, you can have some confidence that access to the Google
account can be recovered in future as people join or leave your team.

## Create a Google project

Once you've determined which account to use, sign in, open the [Google Cloud
console](https://console.cloud.google.com/) and create a new project for
your service; click **Select a project**, then **NEW PROJECT**, enter a name
for the project and, optionally, edit the provided project ID. For projects in
the `cam.ac.uk` Organisation make sure that is selected and change the Location
to your institutional folder (or create a subfolder in that folder first).
Finally click **Create**.

You will first need to add an initial set of administrators:

1. Navigate to the [IAM
   page](https://console.cloud.google.com/iam-admin/iam) of the Google
   developer console for your project.
2. Use the **Add** button at the top of the page to add Lookup groups to the
   project and give them the **Project Owner** role (only the **Project Editor**
   role is permitted for projects outside the `cam.ac.uk` organisation).
   Lookup groups are identified via the email address
   `[groupid]@groups.lookup.cam.ac.uk` where `[groupid]` is the _numeric_ group
   id for the group visible on the Lookup page for that group. Alternatively,
   members may be added individually via their `[crsid]@cam.ac.uk` addresses.
3. If logged in with an ordinary Google account, sign out and proceed using one
   of the Google Workspace accounts you just added as editor to the project (via
   Lookup group or individually).

!!! important "All project administration should now be done as a Google Workspace user"
    It is best practice to use a shared ordinary Google account sparingly. As
    people join or leave your team, their corresponding Google Workspace user
    can be added or removed to the set of project editors, ideally by simple
    Lookup group membership.

    Sign in details for the shared ordinary account should be kept securely for
    disaster recovery purposes or for adding project editors if all existing project
    editors are unavailable.

## Create the client credentials

Once you have a new Google project, you can create some OAuth2 client
credentials for your first application.

1. Open the [Google API Console Credentials
   page](https://console.cloud.google.com/apis/credentials).
2. On the Credentials page, select **Create credentials**, then **OAuth client
   ID**.
3. You may be prompted to set a product name on the Consent screen; if so, click
   **Configure consent screen** and supply the information requested on that
   page. Projects created within the `cam.ac.uk` organisation should use
   "Internal", those outside the organisation ("No Organisation" in Google terms)
   should use "External" user authentication. You will also need to add the
   domain name of any websites providing Raven OAuth2 sign in, and set the
   Authorised Domain to "cam.ac.uk".<br>
   Leave the set of [scopes](apache-oauth2.md#oauth2-scopes) as the default set.
   Click "Save" to return to the Credentials screen.
4. Select **Web Application** for the **Application Type**. If the web
   application or web server you are configuring specifies JavaScript origins or
   redirect URIs enter them here.
5. Click **Create**.
6. On the page that appears, make a note of the **client ID** and **client
   secret**. You will need them to configure your site.

!!! important "Changing the support email address"
    When configuring the Consent screen, you can only select yourself as the
    support email address. It may be necessary to sign in with an ordinary
    Google account (creating first if not already done above) to edit this. If
    this isn't the account used to create the project then you will need at add
    the account as a *Project Editor* beforehand.
