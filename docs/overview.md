title: Overview

# Overview

Raven can be used to authenticate current members of the University.

We currently offer two authentication protocols: [OAuth2](https://oauth.net/2/)
and [SAML 2.0](https://en.wikipedia.org/wiki/SAML_2.0).

This documentation provides hands-on guides for configuring
[Wordpress](wordpress.md) and the [Apache web-server](apache-oauth2.md) to use
Raven for sign in along with some general guidance on how to use Raven OAuth2
and Raven SAML 2.0.

## History

The Raven service was created in 2004 and made use of a University of
Cambridge-specific protocol for authentication on the web. In 2007, Raven SAML
2.0 was launched allowing standards-based authentication to members of
the [UK Access Management Federation](https://www.ukfederation.org.uk/) and to
University websites. In 2019, Raven OAuth2 was enabled given websites in the
University a choice of two standard authentication protocols.

## Protocol comparison

The following table shows which sets of users are supported by the current Raven
protocols.

<table style="table-layout: fixed;">
  <tr>
    <th style="width: 25%;">Raven Protocol</th>
    <th style="width: 25%; text-align: center;">Current University Members</th>
    <th style="width: 25%; text-align: center;">UK Federation Members<sup>1</sup></th>
  </tr>
  <tr>
    <td>OAuth2</td>
    <td style="text-align: center;"><span class="yes">yes</span></td>
    <td style="text-align: center;"><span class="no">no</span></td>
  </tr>
  <tr>
    <td>SAML 2.0</td>
    <td style="text-align: center;"><span class="yes">yes</span></td>
    <td style="text-align: center;"><span class="yes">yes</span></td>
  </tr>
</table>

<div style="font-size: 75%;">
  <sup>1</sup> Requires manual approval and registration of your website with
  the <a href="https://www.ukfederation.org.uk/">UK Access Management
  Federation</a>.
</div>

## This documentation

This documentation is hosted in [a project on the University Developers'
Hub](https://gitlab.developers.cam.ac.uk/uis/devops/raven/docs/). If you find
any errors please [raise an
issue](https://gitlab.developers.cam.ac.uk/uis/devops/raven/docs/issues/new) in
the project.

Contributions are also welcome for this documentation. We're especially
interested in documentation covering different products and web application
frameworks.

Generally we prefer it if you could [open an
issue](https://gitlab.developers.cam.ac.uk/uis/devops/raven/docs/issues/new)
before writing anything to give us a heads up on what you intend to contribute.
That way we can provide some guidance on whether it's the sort of thing we're
looking for and how to proceed.
