title: Golden rules

# The Raven "Golden Rules"

Website security is a topic which is subtle and quick to anger. This pages lists
some "golden rules" to follow when using the Raven service.

## Do not confuse authentication with authorisation

You are probably reading this documentation because you have received a request
to "Raven protect" a website. This simple request embodies a common assumption
that because someone can sign in with Raven (they are *authenticated*) they are
allowed to access a website (they are *authorised*).

Raven can authenticate a great many people. Not all of them have anything to do
with the University. Raven may support authenticating more people in future and
so you should **never** assume that a successful Raven authentication implies
that the user is a member of the University.

That being said, there are a few recipes listed for Raven OAuth2 and Raven SAML
2.0 which you should follow if you want to secure your websites.

## Do not use Raven as a source of identity

As a convenience and as a necessary part of supporting the standard protocols
Raven will forward a subset of a user's [Lookup](https://www.lookup.cam.ac.uk/)
profile. Exactly which subset is forwarded depends on the protocol but you
should *always* view [Lookup](https://www.lookup.cam.ac.uk/) as the definitive
source of information on a user.

There is a separate [guide to interfacing with Lookup](lookup.md) available in
this documentation.

## Use `hd` parameter in Raven OAuth2 requests

Raven OAuth2 can show two login boxes. If you add the `hd=cam.ac.uk` request
parameter to the [login endpoint URL](raven-oauth2.md) then you will get the
usual Raven sign in page.

If you want to authenticate members of the public as well, you can leave off the
`hd` parameter. You will then get the usual Google sign in box. Entering an
`@cam.ac.uk` email address will take you to the Raven sign in page.

## Verify the `hd` and `email` claims when using Raven OAuth2

Most OAuth2 integrations will accept any valid Google-signed token when
configured to use Raven OAuth2. You *must* verify one or, ideally, both of the
following:

* The `hd` claim should be `cam.ac.uk`
* The `email` claim should end with `@cam.ac.uk`

Raven OAuth2 may support authenticating more users in future and failure to
verify the claims may result in more people being able to sign in to your site
than you expect.

## Verify affiliation and/or entitlement attributes when using Raven SAML 2.0

In order to restrict sign in to current members of the University, you *must*
verify that the `urn:oid:1.3.6.1.4.1.5923.1.1.1.9` claim contains
`member@cam.ac.uk`.

Raven SAML 2.0 may support authenticating more users in future and failure to
verify the returned attributes may result in more people being able to sign in
to your site than you expect.

## Do not assume everyone has a CRSid

Raven identifies users via an email-formatted identifier of the form
`[CRSid]@cam.ac.uk`. It is *strongly* recommended that you use the full form of
this identifier to identify users on your site. Using just the local part
a) restricts your site to only those with CRSids and b) may break in subtle ways
if Raven starts authenticating non-`@cam.ac.uk` users in the future.

For example, if you are running a site on behalf of St Botolph's college, not
all of those associated with a college may have CRSids. In such situations it is
useful to allow sign in via your own sign-in system for users with identifiers of
the form `firstname.lastname@botolphs.cam.ac.uk`. If Raven were to start
offering non-CRSid login in future you can then move from your own sign-in
system to Raven without having to re-name all of your users.
